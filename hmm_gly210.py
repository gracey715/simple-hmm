import math
import sys

seq = sys.argv[1]

# Emission probability for exon state
dE = {'A':0.25,'C':0.25,'G':0.25,'T':0.25}
# Emission probability for first base of intron (splice site?)
d1 = {'A':0.05,'C':0.0,'G':0.95,'T':0.0}
# Emission probability for second intron base
d2 = {'A':0.05,'C':0.05,'G':0.1,'T':0.8}
# Emission probability for third intron base
d3 = {'A':0.2, 'C':0.3,'G':0.2,'T':0.3}
# Emission probability for intron state
dI = {'A':0.4,'C':0.1,'G':0.1,'T':0.4}
# Transition probabilities
dTrans = {'EE':0.9,'E5':0.1,'5I':1.0,'II':0.9,'StartE':1.0,'IEnd':0.1}

# Test sequence
seq = 'CTTCATGTGAAAGCAGACGTAAGTCA'

# Initialise dictionary that stores the paths created in the loop below.
# Each path is a pair of state labelling and its probability
paths = {}
path_count = 0

# Create initial start path - just an E initially.
# (this start path will consist only of E's and represents
# the prefix of paths created in the following loop)
paths[0] = [['E'],dTrans['StartE']*dE[seq[0]]]

# Iterate through remaining bases.
# In this simple example, each base position is a possible splice site
# and so gives a new path.
for i, nuc1 in enumerate(seq[1:]): 
    # Ignore impossible paths
    if (dTrans['E5']*d1[nuc1] > 0.0):
        # Add a new possible path ("path" is a state assignment)
        path_count += 1
        paths[path_count] = [list(paths[0][0]),float(paths[0][1])]
        # Extend path with splice site state
        paths[path_count][0].append('5')
        # Update prob of path for this new state (5)
        paths[path_count][1] = paths[path_count][1]*dTrans['E5']*d1[nuc1]
        # Move to second base of intron
        if (seq[1:][i+1:]):
            # Extend by intron state
            paths[path_count][0].append('I')
            # Update prob of path with new intron state
            paths[path_count][1] = paths[path_count][1]*dTrans['5I']*d2[seq[1:][i+1]]
            # Move to third base of intron
            if (seq[1:][i+2:]):
                # Extend by intron state
                paths[path_count][0].append('I')
                # Update prob of path with new intron states
                paths[path_count][1] = paths[path_count][1]*dTrans['II']*d3[seq[1:][i+2]]
                # Rest of sequence
                if (seq[1:][i+3]):
                    # Make rest of states intron label
                    for nuc3 in seq[1:][i+3:]:
                        paths[path_count][0].append('I')
                        paths[path_count][1] = paths[path_count][1]*dTrans['II']*dI[nuc3]
                        # Update final prob with intron to end state transition
                        paths[path_count][1] = paths[path_count][1]*dTrans['IEnd']
    # Extend initial exon labelling by another base if not a possible splice site position
    paths[0][0].append('E')
    # Update the probability of "dummy" exon prefix sequence
    paths[0][1] = paths[0][1]*dTrans['EE']*dE[nuc1] 

# Delete "dummy" all E prefix path 
del paths[0] 
path_sum = 0.0

probs = []
path_list = []

for p in paths:
    # For all paths get the total "probability"
    path_sum = path_sum + paths[p][1] 
    probs.append(math.log(paths[p][1]))
    path_list.append(paths[p][0])

# Get the maximum log(prob)
final_prob = max(probs)
fp_index = probs.index(final_prob)
# Print the path with final maximum likelihood and its log(prob)
print(path_list[fp_index], final_prob)





